import { bindActionCreators } from "../simple-redux/simple-redux";
import React from 'react'

export default connect = (mapStateToProps = state => state, mapDispatchToProps={}) => {
    (WrapComponent) => {
        return class ConnectComponent extends React.component{
            constructor(props, context) {
                super(props, context)
                this.state = {
                    props:{}
                }
                componentDidMount() {
                    const {store} = this.context
                    store.subscribe(() => this.update())
                    this.update()
                }
                update() {
                    const {store} = this.context
                    const stateProps = mapStateToProps(store.getState())
                    const DispatchToProps = bindActionCreators(mapDispatchToProps, store.dispatch)
                    this.setState({
                        props:{
                            ...this.state.props,
                            ...stateProps,
                            ...dispatchProps
                        }
                    })
                }
            }
            render() {
                return <WrapComponent {...this.state.props}></WrapComponent>
            }
        }
    }
}