var Stack = function() {
    this.data = []
}

// push 添加一个元素
Stack.prototype.push = function(e) {
    this.data.push(e)
}

// pop 删除并返回最新添加的元素
Stack.prototype.pop = function() {
    var index = this.data.length - 1
    return this.data.pop()
}

// top 仅返回最新添加的元素
Stack.prototype.top = function() {
    var index = this.data.length - 1
    return this.data[index]
}

// clear 移除栈里所有的元素
Stack.prototype.claer = function() {
    this.data = []
}
// len 返回栈里的元素的个数
Stack.prototype.len = function() {
    return this.data.length
}