// 数组的相邻项去重，区分字母的大小写
const a = 'AAAABBBCCDAABBB'
const b = 'ABBCcAD'   
const c = [1,2,2,3,3]

const uniqueInOrde = (pra) => {
    let array
    if (typeof pra === 'string') {
        array = pra.split('')
    }
    if (Array.isArray(pra) === true) {
        array = pra
    }
    const myArray=[array[0]]
    const len = array.length
    for (let i = 1; i < len; i++) {
      if ( array[i] !== myArray[myArray.length-1])
      {
        myArray.push(array[i])
      }
    }
    return myArray
}

console.log(uniqueInOrde(a))
console.log(uniqueInOrde(b))
console.log(uniqueInOrde(c))
