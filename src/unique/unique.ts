const unique = (array: Array<any>):Array<any> => {
    const newArr = array.filter((item, index, array) => {
        return array.indexOf(item) === index
    })
    return newArr
}