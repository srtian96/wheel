var Queue = function() {
    this.data = []
}
// 入队
Queue.prototype.enqueue = function (e) {
    this.data.push(e)
}

// 出队
Queue.prototype.dequeue = function () {
    return this.data.shift()
}

// 队列长度
Queue.prototype.len = function () {
    return this.data.length
}

// 清空队列
Queue.prototype.clear = function() {
    this.data = []
}