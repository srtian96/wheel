简易版本的redux

现在已实现：
- createStore
- compose
- applyMiddleware
- combineReducer
- bindActionCreators

其中createStore实现了：
- getState
- subscribe
- dispatch