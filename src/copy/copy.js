function clone(Obj) {
  const buf
  if (Obj instanceof Array) {
    buf = []
    let i = Obj.length
    while (i--) {
      buf[i] = clone(Obj[i])
    }
    return buf
  }
  if (Obj instanceof Object && typeof Obj !== 'function') {
    buf = {}
    for (let key in Obj) {
      buf[key] = clone(Obj[key])
    }
    return buf
  }
  else {
    return Obj
  }
}