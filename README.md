# wheel

造轮子的集合，目标是将自己感兴趣的轮子都造上一遍

### 轮子目录（目前计划）：
#### react:
- [x] - simple-redux
- [x] - simple-react-redux
- [ ] - simple-react-router
- [ ] - 虚拟DOM
#### JavaScript
- [x] - promise
- [x] - 队列
- [x] - 栈
- [ ] - 数组去重
- [ ] - bind
- [ ] - extend
- [ ] - assign
- [x] - 深克隆
- [ ] - MVVM
#### node.js
- [ ] - koa